//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by student on 2/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    var activity1:String? = "Bicycling"
    @IBOutlet weak var pickerview: UIPickerView!
    let activity = ["Bicycling","Jumping rope","Running - slow","Running - fast","Tennis","Swimming"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return activity[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activity.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        activity1 = activity[row]
    }
    @IBOutlet weak var ActivityTF: UITextField!
    
    @IBOutlet weak var WeightTF: UITextField!
    
    
    @IBOutlet weak var ExerciseTF: UITextField!
    
    
    
    @IBOutlet weak var EnergyConsumedLBL: UILabel!
    
    
    @IBOutlet weak var TimeLoseLBL: UILabel!
    
    
    @IBOutlet weak var error: UILabel!
    
    @IBAction func Calculate(_ sender: Any) {
        if let act = activity1, let weight = Double(WeightTF.text!), let time = Double(ExerciseTF.text!) {
            EnergyConsumedLBL.text = String(format:"%.1f cal", ExerciseCoach.energyConsumed(during: act, weight: weight, time: time))
            TimeLoseLBL.text = String(format:"%.1f  minutes", ExerciseCoach.timeToLose1Pound(during: act, weight: Double(WeightTF.text!)!))
        } else {
            error.text = "Please enter valid details"
        }
    }
    
    @IBAction func Clear(_ sender: Any) {
        
        activity1! = " "
        WeightTF.text! = " "
        ExerciseTF.text! = " "
        EnergyConsumedLBL.text = "0 cal"
        TimeLoseLBL.text = "0 minutes"
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

